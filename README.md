# README #

Instructions on how to setup the project and information on to where/how to integrate the components

### Setup ###

1. Clone the repository
2. Create a virtual environment
3. Install the requirements

Enter your Stripe API Secret and Publishing Keys into the bottom of product-api/settings.py

```
#!bash

pip install -r requirements.txt
```

## URLs/Endpoints ##

/ - Simple Stripe.js form

/api/product/ - GET existing products or POST to create new Product
/api/product/<pk>/ - GET Product By ID
/api/order/' - GET existing orders or POST to create new Order
/api/order/<pk>/ - GET Order by ID
/api/review/' - GET existing reviews or POST to create new Review
/api/review/<pk>/ - GET Review by ID

## Project Overview ##

### api ###

All of the API related code is contained within this app/module. The implementation of the API is very basic as the 
Django Rest Framework does a lot of the heavy lifting by default.

- `pagination.py` contains 2 serializer classes which aid the pagination specific serialization and includes a object in the
JSON response which contains the URLs for the next and previous pages (where applicable).

- `serializers.py` contains the data serializers for the main body of the API responses. Due to the different response 
output requirements in the supplied documentation, I have created serializers which are used when displaying a list of Products or Orders, and one
when retrieving a single Product or Order. There is also an exception with `OrderPostSerializer` which is used only for POST
requests to /api/order/ as `OrderSerializer` inherits from HyperLinkedModelSerializer which requires foreignkey relations to be supplied as a URL
rather than a ID which seemed really horrible

- views.py contains the API classes. Only thing worth noting is `DynamicSerializerViewSet.get_serializer_class` which overrides the default
behaviour and switches the serializer class to the standard, or list serializer depending on the request. 
Everything else is handled by Django Rest Framework default implementations.

### core ###

Contains the models for the application. As requested, the integration with Stripe resides on the Order Model so on creation of a new into the DB
the charge request is made to Stripe. The reason for putting it within the save was that you'd only be creating orders via the API so it made sense to keep it here.

- `views.py` contains a single view which allows for easy testing of the Order/Stripe integration